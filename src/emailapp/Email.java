package emailapp;

import java.util.Scanner;

public class Email {
	private String firstName;
	private String departmant;
	private String lastName;
	private String password;
	private int mailboxCapacity = 500;
	private String alternateEmail;
	private int defaultPasswordLength = 10;
	private String email;
	private String companySuffix = "company.com";
	
	/*Have to make constructor of person with name and lastName,
	 * program will ask for department
	 * generate password
	 * set a email capacity, alternate email
	 * change the password
	 */
	
	//email constructor
	public Email(String firstName, String lastName) {
		this.firstName= firstName;
		this.lastName = lastName;
		
	//doesen't have to b displayed	System.out.println("Email set: " + this.firstName + " " + this.lastName);
		
		this.departmant = setDepartmant();
	// doesn't need t b displayed	System.out.println("Deparmant: " + this.departmant);
		
		this.password = randomPassword(defaultPasswordLength);
		System.out.println("Your password is: " + this.password);
		
		email = firstName.toLowerCase() + "." + lastName.toLowerCase() + "@" + departmant + "." + companySuffix;
		 System.out.println("Your email is: "+"\n" + email);
		
	}
	//Setting method for department
	
	private String setDepartmant() {
		System.out.print("Choose deparmant\n1 for sales\n2 for development\n3 for financial\n0 for none\nEnter depstment code: ");
	
		Scanner in = new Scanner(System.in);
		int depChoice = in.nextInt();
		if (depChoice == 1) {
			return "sales";
		} else if (depChoice == 2) {
			return "development";
		} else if (depChoice == 3) {
			return "financial";
		} else 
			return"";
	}
	//call a method for rand password
	private String randomPassword(int length) {
		String passwordSet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%";
		char[] password = new char[length];
		for (int i = 0; i<length; i++) {
		int rand =(int) (Math.random() * passwordSet.length());
		password[i] = passwordSet.charAt(rand);
		
		}
	
	return new String(password);
	    }
	public void setMailboxCapacity(int capacity) {
		this.mailboxCapacity = capacity;
	}
	public void setAlternateEmail(String altEmail) {
		this.alternateEmail = altEmail;
	}
	public void changePassword(String password) {
		this.password = password;
	}
	public int getMailboxCapacity() {
		return mailboxCapacity;
	}
	public String getAlternateEmail() {
		return alternateEmail;
		
	}
	public String getPassword() {
		return password;
	}
	//method for all info
	public String showInfo() {
		return "DISPLAY NAME: " + firstName + " " + lastName + 
				"\nCOMPANY EMAIL: " + email +
				"\nMAILBOX CAPACITY: " + mailboxCapacity +"mb";
				}
}
