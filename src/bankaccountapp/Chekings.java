package bankaccountapp;

public class Chekings extends Account {
	
	//list of propertis specific for cheking account
	private int debitCardNumber;
	private int debitCardPIN;
	
	//constructors to init account properties
	public Chekings(String name, String socialNum, double initDeposit) {
		super(name, socialNum, initDeposit);
		accountNumber = 2 + accountNumber;
		setDebitCard();
		 
	}
	@Override
	public void setRate() {
		rate = getBaseRate() * .15;
	}
	
	public void setDebitCard() {
		debitCardNumber = (int) (Math.random() * Math.pow(10, 3));
		debitCardPIN = (int) (Math.random() * Math.pow(10, 4));
	}

	
	//list specific properties
	
	public void showInfo() {
		super.showInfo();
		System.out.println("Your Cheking account feauters: " + 
		                  "\nDebit card number: " + debitCardNumber +
		                  "\nDebit card PIN: " + debitCardPIN);
	}

}
