package bankaccountapp;

public abstract class Account implements IbaseRate{
	
	//list of common properties with checking and saving
	private String name;
	private String socialNum;
	private double balance;
	private static int index = 10000;
	protected String accountNumber;
	protected double rate;
	
	//constructor to init account
	public Account(String name, String socialNum, double initDeposit) {
		this.name = name;
		this.socialNum = socialNum;
		balance = initDeposit;
		
		index++;
	    this.accountNumber = setAccountNumber();
	   
	    setRate();
	
	}
	public abstract void setRate();
	
	private String setAccountNumber() {
		String lastTwoOfSSN = socialNum.substring(socialNum.length()-2, socialNum.length());
		int uniqueID = index;
		int randomNum = (int) (Math.random() * Math.pow(10, 3));
		return lastTwoOfSSN + uniqueID +randomNum;
	}
	public void compound() {
		double accruedInterest = balance * (rate/100);
		balance = balance + accruedInterest;
		System.out.println("Accrued interest: " + accruedInterest);
		printBalance();
	}
	
	//list of common methods
	public void deposit(double amount) {
		balance = balance + amount;
		System.out.println("Depoditing " + amount);
		printBalance();
	}
	
	public void withdraw(double amount) {
		balance = balance - amount;
		System.out.println("Withdrawing " + amount);
		printBalance();
	}
	
	public void transfer(String toWhere, double amount) {
		balance = balance - amount;
		System.out.println("Transfering " + amount + balance);
		printBalance();
	}
	
	public void printBalance() {
		System.out.println("Your balance now is: " + balance);
	}
	
	
	public void showInfo() {
		System.out.println("Name: " + name +
				"\nAccount number: " + accountNumber+
				"\nBalance: " + balance +
				"\nRate: " + rate + "%");
	}
	
	

}
