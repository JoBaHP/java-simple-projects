package bankaccountapp;

import java.util.LinkedList;
import java.util.List;

public class BankAccountApp {

	public static void main(String[] args) {
	    
		List<Account> accounts = new LinkedList<Account>();
	// read CSV file and then create account
		String file =  "C:\\Documents\\Files\\NewBankAccounts.csv";
		
//		Chekings ch1 = new Chekings("Tom Ford", "11111111", 2500);
//		
//		Savings sv1 = new Savings("Jon Week", "22222222", 3500);
//		
//		ch1.showInfo();
//		System.out.println("******************");
//		sv1.showInfo();
		
		List<String[]> newAccountHolders = utilities.ReaderCSV.read(file);
		for (String[] accountHolder : newAccountHolders)  {
			
			
			String name = accountHolder[0];
			String sSN = accountHolder[1];
			String accountType = accountHolder[2];
			double initDeposit = Double.parseDouble(accountHolder[3]);
			System.out.println(name + " " + sSN + " " + accountType + " $" + initDeposit); 
			
			if (accountType.contentEquals("savings")) {
				accounts.add(new Savings(name, sSN, initDeposit));
			} else if (accountType.contentEquals("Cheking")) {
				accounts.add(new Chekings(name, sSN, initDeposit));
			} else {
				System.out.println("error");
			}
		}
		
		for (Account acc : accounts) {
			System.out.println("\n************");
			acc.showInfo();
			
		}
		
		
		

	}

}
