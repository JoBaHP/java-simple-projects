package bankaccountapp;

public class Savings extends Account{
	
	//list of properties specific for saving account
	private int safetyDepositBoxID;
	private int safetyDepositBoxKey;
	
	//constructor to init setings in saving account
	public Savings(String name, String socialNum, double initDeposit) {
		super(name, socialNum, initDeposit);
		accountNumber = 1 + accountNumber;
		
		setSafetyDepositBox();
		
	
		
	}
	private void setSafetyDepositBox() {
		safetyDepositBoxID = (int) (Math.random() * Math.pow(10, 3));
		safetyDepositBoxKey = (int) (Math.random() * Math.pow(10, 4));
		
	}
	
	//list any method specific for saving account
	public void showInfo() {
		
		super.showInfo();
		System.out.println("Your saving account feauters: " + 
		                   "\nSafety deposit box id: " + safetyDepositBoxID +
		                   "\nSafety deposit box key: " + safetyDepositBoxKey );
		
	}
	@Override
	public void setRate() {
		rate = getBaseRate() - .25;
	}

}
