package bankaccountapp;

public interface IbaseRate {
	default double getBaseRate() {
		return 2.5;
	}

}
